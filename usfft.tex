The preceding section presented a method for computing a DFT on nonuniformly spaced time and frequency samples via the so-called NUFFT given in \cite{Candes05fastdiscrete}. These algorithms are also sometimes referred to as unequally spaced fast Fourier transform (USFFT) algorithms. However, this is by no means the only method for such a DFT. The late 20th century saw the advent of a number of different USFFT algorithms. Many of these algorithms are similar to the preceding algorithm in that they combine interpolation schemes with oversampling. Here we will provide a brief overview of a few of the key papers and USFFT algorithms. Some references not discussed in detail here that discuss algorithms for computing a NUFFT include \cite{Duij1999}, \cite{Green2004} and \cite{Potts00fastfourier}. The notation of this section follows the original papers and thus may differ from our earlier discussion.  



\subsection{Interpolation with Gaussian Bells}
Dutt and Rokhlin \cite{Dutt93} presented an algorithm to take a set of $N$ complex numbers $h_k$ and compute sums of the form 
\begin{equation}
\label{eqn:usfft_dutt}
\hat{h}_j = \sum_{k=0}^{N}h_ke^{i x_j w_k}, \;\; j = 0,\ldots,N
\end{equation}
where $w_n \in [-N/2,N/2]$ and $x_l \in [-\pi,\pi]$ are not necessarily uniformly spaced points. The paper by Dutt and Rokhlin actually contains a collection of algorithms for computing sums of this form, and their inverses, in various circumstances. However, here we are only going to present the ideas behind the algorithms and thus the specifics of the given problems are omitted. Please see \cite{Dutt93} for full descriptions of all the algorithms. The presentation here is also in line with the presentation of the paper, which speaks in terms of interpolation schemes. However, as discussed in class there are alternatives to this presentation of the method.

Just as the NUFFT presented in the preceding section uses a combination of interpolation and oversampling, so does the algorithm presented by Dutt and Rokhlin. In particular, a specialized interpolation scheme using Gaussian bells is developed and used to move between nonequispaced points and equispaced points. To achieve this, Rokhlin and Dutt use the fact, Theorem 2.10 in \cite{Dutt93}, that functions of the form $e^{icx}$ may be approximated on some interval of the real line using a small collection of terms of the form $e^{bx^2}e^{ikx}$ with integer $k.$ The number of terms, $q,$ needed is independent of $c.$ To illustrate how these representations may be used, consider writing
\begin{equation}
\label{eqn:approx}
 e^{icx} \approx e^{bx^2}\sum_{k = [c]-q/2}^{[c]+q/2}\rho_k e^{ikx},
\end{equation}
for some $\rho_k$, see, \textit{e.g.}, Corollary 2.9 and Theorem 2.10 in \cite{Dutt93}. Notationally, $[c]$ represents the integer closest to $c$ and the accuracy of such a representation and the interval on which it is valid may be controlled by choosing $q$ and an oversampling factor denoted $m.$ 

These representations allow for the transition between nonequispaced and equispaced grids. Specifically, if the $x_j$ are equispaced and the $w_k$ are not then this representation formula converts \eqref{eqn:usfft_dutt} into a standard DFT  of length $mN$ which may be computed via the FFT. Conversely, if the $x_j$ are not equispaced but the $w_k$ are a formula of the form \eqref{eqn:approx} may be used to interpolate the result of an oversampled DFT, once again computed by the FFT, onto the unequally spaced $x_j.$ These steps analogous to those in the Type I and Type II NUFFT algorithms presented previously.

These observations mean that a NUFFT using this interpolation scheme has two key components, there is an interpolation step and the computation of an FFT of length $mN.$ In particular in \cite{Dutt93} the authors show that the interpolation step only requires $O(Nq)$ operations to achieve a desired accuracy. Thus, based on the $O(mN\log N)$ run time for the FFT the authors show that the operation count when either the $w_k$ are equispaced or the $x_j$ are equispaced is $O(mN\log N + Nq),$ in the case where both are not uniformly spaced this grows to $O(m^2N\log N + Nq).$ The authors assume that $m^2 \ll N,$ and in fact it is often sufficient to take $m$ as a small integer. Finally, the authors use the fact that $q \sim \log(1/\epsilon)$ where $\epsilon$ is the desired accuracy to get an operation count in the first case of $O(mN\log N + N\log(1/\epsilon)).$ 

\subsection{Projecting functions onto a subspace}
Beylkin in \cite{Beylkin1995} presents an alternative USFFT algorithm to the one previously presented. Furthermore, the paper presents a different perspective on the structure of the algorithm. Specifically, instead of considering an interpolation scheme the paper presents a means of projecting a function onto a specific space that allows for the computation of an unequally spaced DFT via a projection, an oversampled DFT, and a final correction step. A brief overview of some of the key ideas in the algorithm are given here, for details please see \cite{Beylkin1995}. Beylkin also discusses the multidimensional case, however, here we will restrict our discussion to the one dimensional case.

Similar to before, let us consider a set of $N_p$ complex numbers $h_k$ for which we wish to compute sums of the form 
\begin{equation}
\label{eqn:usfft_beylkin}
\hat{h}_j = \sum_{k=0}^{N_P-1}h_ke^{-2 \pi i x_k \xi_j}, \;\; j = 0,1\ldots, N
\end{equation}
where $\xi_j \in [-N,N]$ and $x_k \in [0,1]$ are not necessarily uniformly spaced points. To address the problem of rapidly computing sums of this form Beylkin introduces the generalized function 
\begin{equation}
h(x) = \sum_{k=0}^{N_P-1}h_k\delta (x-x_k).
\end{equation}  
Using this the evaluation of \eqref{eqn:usfft_beylkin} may be thought of as computing
\begin{equation}
\label{eqn:usfft_int}
\hat{h}(\xi) = \int h(x)e^{-2\pi i x \xi} dx,
\end{equation}
at the set of points $\xi_j,$ $j = 0,1,\ldots,N$ where $\vert \xi_j \vert \leq N.$ This converts the problem into one where we wish to accurately and rapidly compute the Fourier transform of a generalized function in the region $\vert \xi \vert \leq N.$ 

The first step of the algorithm for computing the necessary Fourier transforms is to project the generalized function $h(x)$ onto the $j^{th}$ level of a set of subspaces each spanned by translations and scaling of a scaling function $\phi(x),$ denoted
\[
\phi_{k,j}(x) = 2^{-j/2}\phi(2^{-j}x-k),
\]
see \cite{Beylkin1995} for details. Considering the projections of $h(x)$ onto $\phi_{k,j}$ yields coefficients
\[
g_k = \int h(x) \phi_{k,j}(x)dx.
\]
Computation of these coefficients may be interpreted as a blurring of the function, and this interpretation is similar to the role of the interpolation in the algorithm by Dutt and Rokhlin. The next step of the algorithm is to consider the Fourier series, 
\begin{equation}
\label{eqn:fs}
H(x) = \sum_{k} g_k e^{-2\pi i \xi k}.
\end{equation}
It is important to note that specifically for the USFFT the compact support of $h(x)$ means that for an appropriate selection of $\phi(x)$ (\textit{e.g.}, compact support) the $g_k$ will only be nonzero for a finite range of $k,$ which depends on $j$ and the support of $\phi.$ This Fourier series, after an appropriate multiplicative correction in $\xi$ space is an accurate approximation for $\hat{h}(\xi)$ on a fixed interval, see Theorem III.1 in \cite{Beylkin1995}. Under an appropriate selection of $\phi(x)$ and an oversampling factor $\nu$ any desired accuracy may be achieved over the region of interest.   

Beylkin motivates choosing  $\phi$ to be the central B-spline of $m^{th}$ order, denoted $\beta^{(m)}(x).$ Essentially, the compact support of the splines in the spatial domain coupled with their decay in the Fourier domain motivates the choice. 

To outline one version of the algorithm we consider the case where the $\xi_j$ are equally spaced and the $x_k$ are not. As before, the algorithms for the other cases are similar in structure. To address the case where the $\xi_j$ are not equally spaced an interpolation scheme using B-splines is used, see \cite{Beylkin1995} for the details. The compact support in space coupled with the nature of the function $h(x)$ means that $g_k$ may be computed as
\[
g_k = 2^{-j/2}\sum_{l=0}^{N_p-1}h_l\beta^{(m)}(2^{-j}x_l-k).
\]
Only a finite number of these coefficients will be nonzero and thus the Fourier series
\[
H(\xi) = \sum_{k}g_k e^{-2\pi i k \xi}
\]
 may be evaluated at the desired equispaced points via the FFT. Multiplying by a corrective factor in $\xi,$ which may be computed cheaply yields an approximation for $\hat{h}_j.$ The projection onto the B-splines costs $mN_p$ operations and the FFT is used on a $\nu N$ length signal. Furthermore, since $m \sim \log(1/\epsilon)$ where $\epsilon$ is the desired accuracy the final computational cost of the algorithm is $O(\log(1/\epsilon)N_p + \nu N\log N).$ As before, we observe that these USFFT algorithms all tend to require both an interpolation, or projection, step and the computation of an oversampled DFT via the FFT, hence the broadly similar computational costs. 
