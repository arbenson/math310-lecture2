So far, we have assumed that the frequencies $w$ in evaluating $F(w)$ were equispaced and that the sampling times $t$ for $x(t)$ were equispaced.
However, in many applications the evaluation points in frequency and/or time are not equispaced yet we would still like to be able to evaluate the trigonometric polynomial at all of the desired points in $O(N \log N)$ time.

\subsection{Applications}
Let us consider the case where the data, $y(k),$ consists of measurements of $F(\omega_k)$ at unequispaced frequencies $\omega_k$ and we want to recover the signal $x(t)$  at equispaced $t.$ By definition these signals satisfy the following relationship:

\begin{equation}
\label{eq:app}
y(k) = \sum_{t = 0}^{N - 1}x(t)e^{-i\omega_k t}, \quad k = 0, \ldots, M-1.
\end{equation}

One specific problem of this form is magnetic resonance imaging (MRI). The frequency samples $y(k)$ are collected by the machine and we want to recover the brain image $x(t).$ 

Another example is computed axial tomography (CAT) scans. In this case, after suitable preprocessing we are given frequency samples $y(k)$ that lie on a polar grid in $2D.$ Note that in this special case there are actually specialized algorithms for computing this so called polar DFT rapidly, see, \textit{e.g.} \cite{Averbuch2006}, \cite{Beylkin2007} and \cite{Fenn2007}.


Another application is finding numerical solutions to partial differential equations such as heat equation (the differential equation $u_t = u_{xx}$), see, \textit{e.g.}, \cite{Greengard2000}.
In this case, the problem is solved in the frequency domain at unequispaced points, as shown in Figure~\ref{fig:unequi_heat}. 

\begin{figure}[h!]
\centering
\includegraphics[scale=0.4]{"./figs/unequi_heat"}
\caption{Unequispaced frequency samples for computing a numerical solution of the heat equation.  Image from \cite{Greengard2000}.}
\label{fig:unequi_heat}
\end{figure}


%% \subsection{Off the grid in the frequency domain}

%% We now consider the case of sampling unequispaced frequencies.
%% In other words, our data consists of $F(\omega_k)$ at unequispaced frequencies $\omega_k$.
%% We know the data $y(k) = F(\omega_k)$ and we want to find the original signal $x(t)$ at equispaced $t$, knowing that they satisfy the following relationship:

%% \[
%% y(k) = \sum_{t = 0}^{N - 1}x(t)e^{-i\omega_k t}, \quad k = 0, \ldots, M-1.
%% \]

\subsection{Least squares formulation}

In matrix form, we can write \eqref{eq:app} as $y = Ax$, where $A_{kt} = e^{-i\omega_k t}$, $k = 0, \ldots, M-1$, $t = 0, \ldots, N-1$.
Typically, $M \ge N$, $y \approx Ax$, and one approach is to solve the following least squares problem:

\[
\min_{x \in \mathbb{C}^n} f(x) = ||y - Ax||_2^2
\]

In practice $A$ is too large for a direct method such as $QR$, so we use an iterative method to solve this optimization problem.
An example iterative method is gradient descent, which requires being able to evaluate the gradient of the objective function $f$:
\[
\nabla f(x) = A^*(Ax - y)
\]

Thus, we are concerned with applying the matrices $A$ and $A^*$ quickly.
Let's examine how these matrices operate on a vector via

\begin{itemize}
\item  $(Ax)_k = \sum_{t = 0}^{N - 1}x(t)e^{-it\omega_k}$ (equispaced $\to$ unequispaced),
\item $(A^*x)_k = \sum_{n = 0}^{M - 1}x(t_n)e^{2\pi it_nk}$ (unequispaced $\to$ equispaced),
\end{itemize}

where we have switched the role of time and space for the adjoint operator. Thus, $A^*A$ takes an equispaced vector to an equispaced vector.
In the language of Greengard and Lee \cite{Green2004}, applying the operator $A$ constitutes a \emph{nonuniform FFT (NUFFT) of Type II} and applying the operator $A^*$ constitutes a \emph{NUFFT of Type I}.
For completeness, we also have the \emph{NUFFT of Type III}, which is used to evaluate $\sum_{n = 0}^{N - 1}x(t_n)e^{-it\omega_k}$ \text{(unequispaced $\to$ unequispaced)}.


Na\"ively, the computational complexity of applying $A$ and $A^*$ is $O(MN)$.
In Section~\ref{sec:victor}, we will examine how to rapidly apply $A$ and $A^*$ to a vector.
We note that, given the $\omega_k$, we have an analytic representation of $A_{kt}$, namely $e^{-i\omega_k t}$.
Thus, $A$ and $A^*$ require only $O(N)$ memory.














