Last time, we learned about rapidly computing the discrete Fourier transform (DFT),
\[
\hat{x}(k) = \sum_{t = 0}^{N-1}x(t)e^{-i2\pi kt/ N},\quad  k = 0, 1, \ldots, N-1,
\]
when $N$ was not prime using the fast Fourier transform (FFT) algorithm.
The $O(N \log N)$ computational complexity of the FFT and inverse FFT (IFFT) give us a fast way of evaluating the response of a linear, time-invariant (LTI) system to a signal.
This is illustrated in the diagram below, where $D$ stands for a diagonal operator.

\begin{center}
\begin{tikzpicture}
  \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
  {
     x & y \\
     \hat{x} & \hat{y} \\
   };
  \path[-stealth]
    (m-1-1) edge node [left] {FFT} (m-2-1)
            edge node [above] {LTI system} (m-1-2)
    (m-2-1.east|-m-2-2) edge node [below] {$D$}
            node [above] {} (m-2-2)
    (m-2-2) edge node [right] {IFFT} (m-1-2);
\end{tikzpicture}
\end{center}

Because the FFT is so important, there has been significant research effort to implement the algorithm in hardware \cite{Baas1999, Cetin1997, He1998}.
However, we need the IFFT to complete the above diagram.
Does this mean we need FFT \emph{and} IFFT hardware?
Of course not.
The trick is time reversal:
\begin{eqnarray*}
x(t) &=& \frac{1}{N}\sum_{k=0}^{N-1} \hat{x}(k)e^{i2\pi k t / N} \\
&=& \frac{1}{N}\sum_{k=0}^{N-1} \hat{x}(N - 1 - k)e^{i2\pi (N - 1 - k) t / N} \\
&=& \frac{e^{-i2\pi t / N}}{N}\sum_{k=0}^{N-1} \hat{x}(N - 1 - k)e^{-i2\pi k t / N}
\end{eqnarray*}

The summation in the last line is the FFT of the time-reversed signal $\hat{x}(N -1  - k)$.
Thus, the IFFT consists of three steps: (1) time reverse, (2) FFT, and (3) post-multiply.


