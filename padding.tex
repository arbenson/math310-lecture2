An important consideration is the effect of zero padding a signal in time before computing a DFT.
Suppose our signal is of length $N$, which we will denote $x_{N}(t)$.
The zero-padded signal of length $2N$, $x_{2N}(t)$, is defined as

\begin{center}
  $x_{2N}(t) = \left\{
     \begin{array}{l l}
       x_N(t), \quad &t = 0, \ldots, N -1 \\
       0, \quad  &t = N, \ldots, 2N - 1
     \end{array}
   \right.$
\end{center}

The DFT of $x_{2N}(t)$ is then
\begin{eqnarray*}
\hat{x}_{2N}(k) &=& \sum_{t = 0}^{2N - 1}x_{2N}(t)e^{-2\pi i t k / (2 N)} \\
&=& \sum_{t = 0}^{N - 1}x_{2N}(t)e^{-2\pi i t k / (2 N)} \\
\end{eqnarray*}

This is almost the DFT of $x_{N}$.
The difference is the $2N$ in the exponential and the fact that $k$ ranges from $0$ to $2N - 1$.
We seek a physical interpretation of this DFT.
To do this, we view the DFT as a trigonometric polynomial:

\[
\hat{x}_N(k) = \sum_{t = 0}^{N - 1}x_N(t)e^{-i2\pi k t / N} \rightarrow F(w) = \sum_{t = 0}^{N - 1}x_N(t)e^{-itw}
\]

The DFT of the signal $x_N$ is evaluating the polynomial $F(w)$ at $w = \frac{2 \pi k}{N}$, $k = 0, \ldots, N-1,$ the FFT provides a fast algorithm for this evaluation.
We can view this as sampling at the Nyquist rate in the frequency domain, see Figure~\ref{fig:nyq}.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[xscale=15]
%N = 8
\draw[-][draw=black, thick] (0,0) -- (1,0);
\draw [thick] (0,-.1) node[below]{0} -- (0,0.1);
\draw [thick] (1/8, -.1) node[below]{$\frac{2\pi}{N}$} -- (1/8,0.1);
\draw [thick] (2/8, -.1) node[below]{$\frac{4\pi}{N}$} -- (2/8,0.1);

\foreach \x in {3,4,5,6}
{
	\draw [thick] (\x/8,-.1) node[below]{\dots} -- (\x/8,0.1);
		}
\draw [thick] (7/8, -.1) node[below]{$\frac{\pi(2N-1)}{N}$} -- (7/8,0.1);
\draw [thick] (1, -.1) node[below]{$2\pi$} -- (1,0.1);
\end{tikzpicture}
\caption{Samples in the frequency domain with no zero-padding (Nyquist rate).\label{fig:nyq}}
\end{figure}

Through this lens, the DFT of $x_{2N}(t)$ is evaluating the same polynomial $F(w)$ at additional points, namely, $w = \frac{\pi k}{N}, k = 0, \ldots, 2N-1$.
This is illustrated in Figure~\ref{fig:nyq2}.
We can similarly evaluate the DFT of $x_{dN}(t)$, and we say that $d$ is the \emph{oversampling factor}.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[xscale=15]
%Nd = 24
\draw[-][draw=black, thick] (0,0) -- (1,0);
\draw [thick] (0,-.1) node[below]{0} -- (0,0.1);
\draw [thick] (1/16, -.1) node[below]{$\frac{2\pi}{2N}$} -- (1/16,0.1);
\draw [thick] (2/16, -.1) node[below]{$\frac{4\pi}{2N}$} -- (2/16,0.1);

\foreach \x in {3,4,...,14}
{
	\draw [thick] (\x/16,-.1) node[below]{\dots} -- (\x/16,0.1);
		}
\draw [thick] (15/16, -.1) node[below]{$\frac{2\pi(N-1)}{N}$} -- (15/16,0.1);
\draw [thick] (1, -.1) node[below]{$2\pi$} -- (1,0.1);
\end{tikzpicture}
\caption{Samples in the frequency domain with an oversampling factor of $d=2$.\label{fig:nyq2}}
\end{figure}

