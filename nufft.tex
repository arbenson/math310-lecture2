The original Dutt \& Rokhlin approach to the NUFFT in \cite{Dutt93} is ``brilliant" (Cand\`es), but requires a lot of background knowledge in signal processing and spectrum analysis. We will review the details in Section \ref{sec:usfft}, but first we look here at a conceptually straight-forward approach by Cand\`es, Demanet, Donoho, \& Ying \cite{Candes05fastdiscrete}.  We will start with the Type II NUFFT, as it is simpler.
\subsection{Algorithm for Type II NUFFT}
Recall that for the Type II NUFFT we wish to evaluate the trigonometric polynomial $F(\omega) = \sum x(t)\exp(-it\omega)$ on an arbitrary grid in frequency space.   In the notation of Section \ref{sec:padding}, suppose we oversample our $N$-point signal $X_N(t)$ by a factor of $d$ by appending $(d-1)N$ trailing zeros to $X_N(t)$,  i.e.,
\begin{align*}
X_{dN}(t) = \left\{\begin{array}{l l}
X_N(t)&:t\in\{t_0,\dots,t_{N-1}\},\\0&:t\in\{t_N,\dots,t_{dN-1}\}.
\end{array}\right.
\end{align*}
As we saw before, this corresponds to evaluating $F(\omega)$ at the regular grid points $\tilde \omega_k = \frac{2\pi k}{dN}$ for $k=0,\dots,dN-1$.

The key idea for computing the Type II NUFFT will be Taylor approximation.  In the first step, on the fine grid we evaluate the trigonometric polynomial and its derivatives, $F^{(l)}(\omega)$ for $l=0,\dots,L-1$.  In the second step, for each point $\omega_k$ in the set of evaluation points we perform a Taylor expansion of order $L-1$ around its nearest neighbor in the fine grid.
\subsubsection{Step 1}
We wish to determine the values $F^{(l)}(\tilde \omega_k)$ for $\tilde \omega_k$ in the fine grid and $l = 0, \dots, L-1$.  For $l=0$, we see that this is just the $dN$-point DFT of $x(t)$, so we can use the Cooley-Tukey FFT algorithm \cite{CTFFT} for regular grids.  For $l\ne 0$, we are dealing with some derivative of $F(w)$, but from the definition we see that
\begin{align*}
F^{(l)}(\omega) &= \frac{\partial^{(l)}}{\partial \omega^{(l)}}\left [ \sum x(t)\exp(-it\omega)\right]\\
&=\sum x(t) (-it)^{l}\exp(-it\omega)\\
&\equiv \sum x_l(t) \exp(-it\omega),
\end{align*}
where we observe that the last quantity is just the DFT of $x_l(t) = (-it)^l x(t)$, which can be computed via pointwise multiplication (to obtain $x_l(t)$) followed by the Cooley-Tukey algorithm.

Thus, we see that the dominant work in this step is $L$ FFTs, each of length $dN$, for a cost of $O(LdN\log(dN)) = O(LdN\log N)$ (assuming $d \ll N$).
\subsubsection{Step 2}
At the end of Step 1 we have the values of $F(\omega)$ and its derivatives on the fine grid.  The next step is to perform a Taylor expansion for each evaluation point.  Suppose $\omega_k$ is a point in the non-uniform grid at which we want to evaluate $F(\omega)$.  Then, choosing $\tilde \omega_k$ to be the closest fine grid point to $\omega_k$, we can Taylor expand around $\tilde \omega_k$ to obtain
\begin{align*}
F(\omega_k) &\approx F(\tilde \omega_k) + F'(\tilde \omega_k)(\omega_k-\tilde\omega_k) + ... + \frac{F^{(L-1)}(\tilde \omega_k)(\omega_k-\tilde\omega_k)^{(L-1)}}{(L-1)!}\\ &\equiv [P_{l,\tilde \omega_k} f](\omega_k).
\end{align*}
It is evident that, since we have already computed all the necessary function evaluations, this is $\mathcal{O}(LdN)$.
\subsubsection{Error Analysis}
The only approximation made in the above algorithm occurs in the Taylor expansions of Step 2.  Luckily, the error term here is not too difficult to deal with.  Using the Lagrange form of the remainder term for the Taylor expansion, we see that, for any given evaluation point $\omega_k$ the bound below holds:
\begin{align}
\label{eq:rem}
|F(\omega_k) -   [P_{l,\tilde \omega_k} f](\omega_k)| &\le ||F^{(L)}||_\infty \frac{|\omega_k-\tilde\omega_k|^L}{L!}.
\end{align}

To get a better handle on this bound, we employ the following fact about trigonometric polynomials.
\begin{theorem}[\emph {Bernstein's Inequality} \cite{zygmund}]\label{thm:b}
Let $p(z)$ be a trigonometric polynomial of degree $n$ with frequencies ranging from $-n/2$ to $n/2$.  Then we have that
\begin{align*}
||p'||_\infty \le \frac{n}{2}||p||_\infty.
\end{align*}
\end{theorem}

By using Theorem \ref{thm:b} $L$ times on \eqref{eq:rem} we obtain the bound
\begin{align*}
|F(\omega_k) -   [P_{l,\tilde \omega_k} f](\omega_k)| &\le \left(\frac{N}{2}\right)^L\frac{\vert \omega_k - \tilde \omega_k\vert ^L}{L!}||F||_\infty.
\end{align*}
Further, noting that the distance between an evaluation point and its nearest neighbor in the fine grid can never be greater than $\frac{\pi}{dN}$, half the spacing of the fine grid, we obtain the final relative error bound
\begin{align*}
\max_{\omega_k} |F(\omega_k) -   [P_{l,\tilde \omega_k} f](\omega_k)| &\le \left(\frac{\pi}{2d}\right)^L\frac{1}{L!}||F||_\infty.
\end{align*}

For $L = 10$ and $d=4$, the relative error is roughly $2.4\times 10^{-11}$, guaranteeing 11 digits of accuracy.  For most applications, noise in the initial data means this level of accuracy is meaningless anyway, so a more reasonable set of parameters might be $L=4$ and $d=8$, giving a relative error of about $6.2\times 10^{-5}$, guaranteeing 5 digits of accuracy.
\subsection{Example implementation}
The MATLAB code below provides an example implementation of the Type II NUFFT algorithm from \cite{Candes05fastdiscrete}.  Note that some tests will cause the error bound to appear to fail, but this is because we do not know the true value of $||F||_\infty$, so we cannot calculate the true relative error, only an approximation.
\begin{Verbatim}[frame=single]
N = 2^10; L = 4; d = 8;

%Create x, keep track of approximate infinity norm of dN-point FFT of x
t = (0:N-1)';
td = (0:d*N-1)';
x = cos(5*pi*t/N) + 2*cos(20*pi*t/N);
approx_norm = 2*d*N;

%zero-pad, then pointwise multiply to get x_l for each l
xL = [x; zeros((d-1)*N,1)];
for l = 1:L-1
    xL(:,l+1) = (-i.*td).*xL(:,l);
end
F = fft(xL);

%evaluation points
wk = 2*pi*sort(rand(N,1));
wk_tilde = 2*pi*(0:d*N-1)'/d/N;
%This is not a good way to find the nearest neighbor in practice
dist = @(w) find(abs(w-wk_tilde) == min(abs(w - wk_tilde)));
neighbors_idx = arrayfun(dist,wk);
neighbors = wk_tilde(neighbors_idx);
diffs = wk - neighbors;

%true solution at scattered points
ft = @(w) sum(x.*exp(-i*w*t));
Fwk = arrayfun(ft,wk);

%approximate solution from NUFFT Taylor approximation
l = (0:L-1);
taylor = @(neighbor,diff) sum(F(neighbor,:).*diff.^l./factorial(l));

Fwk_approx = arrayfun(taylor,neighbors_idx,diffs);
err = norm(Fwk - Fwk_approx,'inf') /approx_norm;
bnd = abs(pi/2/d)^L / factorial(L);

disp('Maximum error relative to approximate infinity norm of F');
disp(err);
disp('Bound on relative error (depends on true infinity norm of F)');
disp(bnd);
\end{Verbatim}
\subsection{The adjoint operator: Algorithm for Type I NUFFT}
Given that the Type I NUFFT is simply the adjoint of the Type II NUFFT, (i.e., $Y(k) = \sum_{t_n} x(t_n)\exp(ikt_n)$), it should be unsurprising that we can use the adjoint operators in each of the above steps to obtain a Type I algorithm.

As a brief diversion, let's look first at how we can write the Type I algorithm in matrix-vector form.  Let $x\in\mathbb{C}^N$ be the vector of time-domain samples. We introduce the zero-padding operator $Z$, such that
\begin{align*}
x_{d} &= Zx \equiv \left[\begin{array}{c} I\\0 \end{array}\right]x,
\end{align*}
where the top block is an identity of size $N\times N$ and the bottom block is a block of zeros of size $(d-1)N\times N$.  Next, we introduce the operator $M \in \mathbb{C}^{dNL\times dN}$, which performs pointwise multiplication  to obtain a vector of the stacked signals $x_l(t)$, i.e.,
\begin{align*}
\left[ \begin{array}{c}x \\ x_1 \\ \vdots \\ x_{L-1}\end{array}\right] &= Mx_d \equiv
\left[ \begin{array}{c}I\\ D\\D^2 \\ \vdots \\ D^{L-1} \end{array}\right]x_d,
\end{align*}
where $D\in\mathbb{C}^{dN \times dN}$ is given by $D=\text{diag}(-it_n)$.

With $W$ as the DFT matrix, we obtain 
\begin{align*}
\left[ \begin{array}{c}\tilde F \\ \tilde F' \\ \vdots \\ \tilde F^{(L-1)}\end{array}\right] &=\left[\begin{array}{cccc}W& 0 & \dots & 0\\
0 & W & \dots & 0\\
\vdots & \vdots &\ddots & \vdots\\
0 & 0 & \dots & W\end{array} \right]\left[ \begin{array}{c}x \\ x_1 \\ \vdots \\ x_{L-1}\end{array}\right], 
\end{align*}
where each $\tilde F^{(l)}$ is a $dN$-vector of the values of $\tilde F^{(l)}(\omega)$ on the fine grid.  Finally, defining $T_l\in\mathbb{C}^{dN\times dN}$ to be the matrix that introduces the necessary distance weighting for the Taylor polynomial, i.e.,
\begin{align*}
(T_l)_{ij} &= \left \{ \begin{array}{ll} \frac{(\omega_i - \tilde \omega_j)^l}{l!}&\tilde \omega_j\text{ is the closest fine-grid point to }\omega_i, \\  0& \text{else}, \end{array}\right.
\end{align*}
we obtain
\begin{align*}
F &= \left [\begin{array}{cccc}T_0 & T_1 & \dots & T_{L-1}\end{array} \right]\left[ \begin{array}{c}\tilde F \\ \tilde F' \\ \vdots \\ \tilde F^{(L-1)}\end{array}\right],
\end{align*}
where $F$ is the vector of $F(w)$ evaluated at the scattered points in the frequency domain $\omega_k$.  Chaining all the operators together, we can write
\begin{align*}
F &= \left [\begin{array}{cccc}T_0 & T_1 & \dots & T_{L-1}\end{array} \right]\left[\begin{array}{cccc}W& 0 & \dots & 0\\
0 & W & \dots & 0\\
\vdots & \vdots &\ddots & \vdots\\
0 & 0 & \dots & W\end{array} \right]\left[ \begin{array}{c}I\\ D\\D^2 \\ \vdots \\ D^{L-1} \end{array}\right]\left[\begin{array}{c} I\\0 \end{array}\right]x,
\end{align*}
and from here it is evident that to perform the Type I transform, the adjoint of the Type II, we can simply take the adjoint of each of these individual operators, which yields
\begin{align}\label{eq:adj}
Y & = \left [\begin{array}{cc}I & 0 \end{array}\right]\left[ \begin{array}{ccccc} I  &D^* & (D^*)^2 & \dots & (D^*)^{L-1}\end{array}\right]\left[\begin{array}{cccc}W^*& 0 & \dots & 0\\
0 & W^* & \dots & 0\\
\vdots & \vdots &\ddots & \vdots\\
0 & 0 & \dots & W^*\end{array} \right] \left[ \begin{array}{c}T_0^* \\ T_1^* \\ \vdots \\ T_{L-1}^* \end{array}\right]x.
\end{align}
From an algorithmic standpoint, we are done, but let's try to gain some mathematical insight into what this adjoint transformation actually is doing as far as approximations go.
\subsubsection{Mathematical intuition}
Recall that the adjoint transform is
\begin{align*}
Y(k) = \sum_{t_n}x(t_n)\exp(it_nk).
\end{align*}
By Taylor's Theorem, note that for $t$ sufficiently close to $t_n$, we have that
\begin{align*}
\exp(it_nk)  = \exp(itk)\exp(i(t_n-t)k) \approx \exp(itk)\left[ 1 + i(t_n-t)k + \dots + \frac{(i(t_n-t)k)^{L-1}}{L!}\right].
\end{align*}
Let $\tilde t_m$ be the uniform fine-grid points obtained via oversampling.  Then, using some careful reordering,
\begin{align*}
Y(k) \approx \sum_{l=0}^L {(ik)^l}\sum_{\tilde t_m}\exp(i\tilde t_mk)\sum_{t_n \in \mathcal{N}(\tilde t_m)} \frac{(t_n-\tilde t_m)^l}{l!}x(t_n),
\end{align*}
where we note that it is possible for one $\tilde t_m$ to have multiple $t_n$ for which it is the nearest neighbor in the fine grid, and thus the third sum aggregates these terms. 
This suggests the following algorithm \cite{Candes05fastdiscrete}:
\begin{enumerate}
\item Compute the $dN$-vector $x_l(\tilde t_m) = \sum_{t_n \in \mathcal{N}(\tilde t_m)} \frac{(t_n-\tilde t_m)^l}{l!}x(t_n)$ for all $m$ for $l=0,\dots,L-1$.
\item Compute the $dN$-vector $\hat x_l(k)$ by taking the IFFT of $x_l$ for each $l$.
\item Compute the $dN$ vector $\tilde Y(k) = \sum_{l=0}^L {(ik)^l}\hat x_l(k)$.
\item Extract the first $N$ entries of $\tilde Y(k)$.
\end{enumerate}
This is exactly the process described in Eqn. \eqref{eq:adj}.  The error and cost analysis are the same as in the forward case.
\subsection{Type III NUFFTs}
Note that there is nothing of major interest to say about the Type III NUFFT (mapping between nonuniform time and nonuniform frequency) as it is obtained by the same ideas as exposited above: Taylor expand to map to a uniform grid, use the FFT algorithm, then Taylor expand to map to the new nonuniform grid.

